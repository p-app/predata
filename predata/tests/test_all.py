# coding=utf-8
from pytest import raises

from predata.coerce import as_bool, as_float, as_int, as_none, as_str
from predata.string import lower_string, remove_spaces, strip_string, \
    strip_text


def test_strip_text():
    assert strip_text("""

    Lorem ipsum
    """) == "Lorem ipsum"
    assert strip_text("""

    Тест
    """) == "Тест"
    assert strip_text(None) == ''


def test_strip_string():
    assert strip_string(" Lorem ipsum ") == 'Lorem ipsum'
    assert strip_string("   Тест") == 'Тест'
    assert strip_string("  Тест".encode('utf8')) == 'Тест'.encode(
        'utf8',
    )
    assert strip_string(None) == ''


def test_remove_spaces():
    assert remove_spaces(None) is None
    assert remove_spaces(' 1 2 3 ') == '123'
    assert remove_spaces(' т е с т ') == 'тест'
    assert remove_spaces(True) is True


def test_lower_string():
    assert lower_string('TEST') == 'test'
    assert lower_string(0) == 0


def test_as_int():
    assert as_int('0') == 0
    assert as_int(' 0 ') == 0
    assert as_int(1) == 1
    assert as_int(True) == 1

    with raises(TypeError):
        as_int(None)

    with raises(ValueError):
        # float-like string can not be coerced to int
        as_int('2.3')

    with raises(ValueError):
        as_int('test')

    assert as_int(None, try_only=True) is None
    assert as_int('2.3', try_only=True) == '2.3'
    assert as_int('test', try_only=True) == 'test'


def test_as_float():
    assert as_float('2,3') == 2.3
    assert as_float(' 2.3 ') == 2.3
    assert as_float(4) == 4.0
    assert as_float(True) == 1.0

    with raises(TypeError):
        as_float(None)

    with raises(ValueError):
        as_float('test')

    assert as_float('test', try_only=True) == 'test'
    assert as_float(None, try_only=True) is None


def test_as_none():
    assert as_none('null') is None
    assert as_none(' none ') is None
    assert as_none('nil ') is None

    with raises(TypeError):
        as_none(2)

    with raises(ValueError):
        as_none('test')

    assert as_none('test', try_only=True) == 'test'
    assert as_none(None, try_only=True) is None


def test_as_bool():
    assert as_bool(' false') is False
    assert as_bool('none') is False
    assert as_bool('true') is True
    assert as_bool('test') is True
    assert as_bool(0) is False
    assert as_bool(None) is False
    assert as_bool(1) is True
    assert as_bool([]) is False

    with raises(TypeError):
        as_bool('none', none_to_false=False)


def test_as_str():
    assert as_str(0) == '0'
    assert as_str(True) == 'True'
    assert as_str(None) == ''
